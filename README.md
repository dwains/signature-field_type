# Signature Field Type

*redtally.field_type.signature*

#### A signature pad field type.
![Screenshot](screenshot.png?raw=true)

The signature field type provides a HTML5 canvas signature input based on 
[signature_pad](https://github.com/szimek/signature_pad).

### Installation
`composer require redtally/signature-field_type` or add `redtally/signature-field_type: "~1.0.0"` to  your composer.json file.

### Configuration
Below is the full configuration with default values.
```
"example" => [
    "type"   => "redtally.field_type.signature",
    "config" => [
        "mode"                      => "database",
        "canvas_width"              => 480,
        "canvas_height"             => 320,
        "dot_size"                  => null,
        "min_width"                 => 0.5,
        "max_width"                 => 2.5,
        "throttle"                  => 16,
        "min_distance"              =>  5,
        "background_color"          => "#ffffff",
        "pen_color"                 => "#000000",
        "velocity_filter_weight"    => 0.7
    ]
]
```

Key | Example | Description
--- | --- | ---
mode | database | The storage mode for the field type.
canvas_width | 480 | The width of the canvas (signature pad requires a fixed width in order to render correctly).
canvas_height | 320 | The height of the canvas (signature pad requires a fixed width in order to render correctly).
dot_size | 2.0 | The dot size. Leave default to allow signature pad to determine. [Signature Pad Option]
min_width | 1.0 | Minimum width of a line. [Signature Pad Option]
max_width | 3.0 | Maximum width of a line [Signature Pad Option]
throttle | 10 | Draw the next point at most once per every x milliseconds. Set it to 0 to turn off throttling.
min_distance | 6 | Add the next point only if the previous one is farther than x pixels. [Signature Pad Option]
background_color | #f6f6f6 | Color used to clear the background. [Signature Pad Option]
pen_color | #d5d5d5 | Color used to draw the lines. [Signature Pad Option]
velocity_filter_weight | 1.0 | Weight used to modify new velocity based on the previous velocity. [Signature Pad Option]

### Usage
#### Setting Values
Setting the field value programmatically requires a JSON encoded array of canvas points.
e.g:
```
$example->signature = "[[{"x":124.5,"y":67.20001220703125,"time":1519873747894,"color":"black"}]]";
```

#### Basic Output
The default value returned by field type will be the JSON encoded array of canvas points.
```
$example->signature; // "[[{"x":124.5,"y":67.20001220703125,"time":1519873747894,"color":"black"}]]"
```

#### Presenter Output
This section will show you the methods provided by the `Redtally\SignatureFieldType\SignatureFieldTypePresenter` class.

##### SignatureFieldTypePresenter::base64()
This method returns the base64 encoded string version of the signature.

Returns `string`

###### Arguments
Key | Required | Default | Description
--- | --- | --- | ---
$width | false | The field config value of `canvas_width` | The width of the image returned.
$height | false | The field config value of `canvas_height` | The height of the image returned.

###### Example
```
$decorated->signature->base64(); returns data:base64,...
```

```
{{ $decorated->signature->base64 }}
```

##### SignatureFieldTypePresenter::image()
This method returns the instance of \Intervention\Image\Image of the filled canvas.

Returns `\Intervention\Image\Image`

###### Arguments
Key | Required | Default | Description
--- | --- | --- | ---
$width | false | The field config value of `canvas_width` | The width of the image returned.
$height | false | The field config value of `canvas_height` | The height of the image returned.

###### Example
```
$decorated->signature->image(); returns data:base64,...
```

```
{{ $decorated->signature->image->encode('jpg', 75) }}
```