<?php

return [
    'mode'          => [
        'type'     => 'anomaly.field_type.select',
        'required' => true,
        'config'   => [
            'default_value' => config('redtally.field_type.signature::storage.mode', 'database'),
            'options'       => [
                'database'   => 'redtally.field_type.signature::config.mode.option.database',
            ],
        ],
    ],
    'canvas_width' => [
        'type' => 'anomaly.field_type.integer',
        'required' => true,
        'config' => [
            'default_value' => 480
        ]
    ],
    'canvas_height' => [
        'type' => 'anomaly.field_type.integer',
        'required' => true,
        'config' => [
            'default_value' => 320
        ]
    ],
    'dot_size' => [
        'type' => 'anomaly.field_type.decimal',
        'required' => false
    ],
    'min_width' => [
        'type' => 'anomaly.field_type.decimal',
        'required' => true,
        'config' => [
            'default_value' => 0.5
        ]
    ],
    'max_width' => [
        'type' => 'anomaly.field_type.decimal',
        'required' => true,
        'config' => [
            'default_value' => 2.5
        ]
    ],
    'throttle' => [
        'type' => 'anomaly.field_type.integer',
        'required' => true,
        'config' => [
            'default_value' => 16,
            'min' => 0
        ]
    ],
    'min_distance' => [
        'type' => 'anomaly.field_type.integer',
        'required' => true,
        'config' => [
            'default_value' => 5,
            'min' => 0
        ]
    ],
    'background_color' => [
        'type' => 'anomaly.field_type.colorpicker',
        'required' => true,
        'config' => [
            'default_value' => '#ffffff'
        ]
    ],
    'pen_color' => [
        'type' => 'anomaly.field_type.colorpicker',
        'required' => false,
        'config' => [
            'default_value' => '#000000'
        ]
    ],
    'velocity_filter_weight' => [
        'type' => 'anomaly.field_type.decimal',
        'required' => true,
        'config' => [
            'default_value' => 0.7
        ]
    ]
];
