(function (window, document) {

    const fields = Array.from(
        document.querySelectorAll('canvas[data-provides="redtally.field_type.signature"]')
    );

    fields.forEach(function (field) {

        var options = JSON.parse(field.getAttribute('data-signaturepad-options'));
        options.onEnd = function() {
            this._canvas.previousElementSibling.value = JSON.stringify(this.toData());
        };

        field.signature = new SignaturePad(field, options);
        if (field.previousElementSibling.value !== '') {
            field.signature.fromData(JSON.parse(field.previousElementSibling.value));
        }

        if (!field.getAttribute('readonly') && !field.getAttribute('disabled')) {
            var button = field.nextElementSibling;

            button.addEventListener('click', function(e) {
                e.preventDefault();
                this.previousElementSibling.signature.clear();
                this.previousElementSibling.previousElementSibling.value = '';
            });
        } else {
            field.signature.off();
        }
    });

})(window, document);