<?php

return [
    'mode'          => [
        'name' => 'Mode',
        'instructions' => 'The save mode for the field type.',
        'option' => [
            'database' => 'Database (Points)'
        ]
    ],
    'canvas_width' => [
        'name' => 'Canvas Width',
        'instructions' => 'The width of the HTML5 canvas.'
    ],
    'canvas_height' => [
        'name' => 'Canvas Height',
        'instructions' => 'The height of the HTML5 canvas.'
    ],
    'dot_size' => [
        'name' => 'Dot Size',
        'instructions' => 'Radius of a single dot.'
    ],
    'min_width' => [
        'name' => 'Minimum Width',
        'instructions' => 'Minimum width of a line.'
    ],
    'max_width' => [
        'name' => 'Maximum Width',
        'instructions' => 'Maximum width of a line'
    ],
    'throttle' => [
        'name' => 'Throttle',
        'instructions' => 'Draw the next point at most once per every x milliseconds. Set it to 0 to turn off throttling.'
    ],
    'min_distance' => [
        'name' => 'Minimum Distance',
        'instructions' => 'Add the next point only if the previous one is farther than x pixels.'
    ],
    'background_color' => [
        'name' => 'Background Color',
        'instructions' => 'Color used to clear the background.'
    ],
    'pen_color' => [
        'name' => 'Pen Color',
        'instructions' => 'Color used to draw the lines.'
    ],
    'velocity_filter_weight' => [
        'name' => 'Velocity Filter Weight',
        'instructions' => 'Weight used to modify new velocity based on the previous velocity.'
    ]
];
