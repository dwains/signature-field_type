<?php namespace Redtally\SignatureFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldType;

/**
 * Class SignatureFieldType
 * @package Redtally\SignatureFieldType
 */
class SignatureFieldType extends FieldType
{
    /**
     * The database column type.
     *
     * @var string
     */
    public $columnType = 'text';

    /**
     * @var string
     */
    public $class = 'field-signature';

    /**
     * Set the value.
     *
     * @param  $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = !empty(json_decode($value)) ? $value : null;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getValue()
    {
        return !empty(json_decode($this->value)) ? $this->value : null;
    }

    /**
     * Return the input view.
     *
     * @return string
     */
    public function getInputView()
    {
        return 'redtally.field_type.signature::input';
    }

    /**
     * Return the input mode.
     *
     * @return string
     */
    public function mode()
    {
        return $this->config('mode') ?: config('redtally.field_type.signature::storage.mode', 'database');
    }

    /**
     * @return array
     */
    public function getPadOptions()
    {
        $configValues = array_only($this->getConfig(), [
            'dot_size',
            'min_width' ,
            'max_width',
            'throttle',
            'min_distance',
            'background_color',
            'pen_color',
            'velocity_filter_weight'
        ]);

        $options = [];

        foreach ($configValues as $key => $value) {
            $options[camel_case($key)] = $value;
        }

        return $options;
    }
}
