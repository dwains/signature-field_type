<?php namespace Redtally\SignatureFieldType;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypePresenter;
use Anomaly\Streams\Platform\Image\Image;
use Intervention\Image\ImageManager as Intervention;
use Collective\Html\HtmlBuilder;

/**
 * Class SignatureFieldTypePresenter
 * @package Redtally\SignatureFieldType
 */
class SignatureFieldTypePresenter extends FieldTypePresenter
{

    /**
     * The HTML builder.
     *
     * @var HtmlBuilder
     */
    protected $html;

    /**
     * The anomaly Image class.
     *
     * @var Image
     */
    protected $image;

    /**
     * SignatureFieldTypePresenter constructor.
     * @param HtmlBuilder $html
     * @param Image $image
     * @param $object
     */
    public function __construct(HtmlBuilder $html, Image $image, $object)
    {
        $this->html = $html;
        $this->image = $image;
        parent::__construct($object);
    }

    /**
     * @param int $width
     * @param int $height
     * @return null|string
     */
    public function base64($width = null, $height = null)
    {

        if (!$image = $this->image($width, $height)) {
            return null;
        }

        return $image->encode('data-url');
    }

    /**
     * @param int $width
     * @param int $height
     * @return \Intervention\Image\Image|null
     */
    public function image($width = null, $height = null)
    {
        if (!$value = $this->object->getValue()) {
            return null;
        }

        $config = $this->getObject()->getConfig();

        if (is_null($width)) {
            $width = $config['canvas_width'];
        }

        if (is_null($height)) {
            $height = $config['canvas_height'];
        }

        $canvas = (new Intervention())->canvas($width, $height, $config['background_color']);
        $points = json_decode($value, true)[0];

        for ($x = 0; $x < (count($points) - 1); $x++) {
            $next = $x + 1;
            $canvas->line(
                $points[$x]['x'],
                $points[$x]['y'],
                $points[$next]['x'],
                $points[$next]['y'],
                function ($draw) use ($config) {
                    $draw->color($config['pen_color']);
                }
            );
        }

        return $canvas;
    }
}
