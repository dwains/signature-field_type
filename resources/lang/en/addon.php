<?php

return [
    'title'       => 'Signature',
    'name'        => 'Signature Field Type',
    'description' => 'A signature field type.'
];
